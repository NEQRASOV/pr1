# Практическая работа 1
## Создать 2 виртуальные машины на базе ОС Debian 12
Создаем виртуальные машины 

![IMAGE_DESCRIPTION](установка_машин.png)


## Обеспечить между ними сетевой обмен
Проверяем доступность машин

![IMAGE_DESCRIPTION](ПИНГ.png)

## Включить на 1й из ВМ передачу логов по протоколу rsyslog на 2ю ВМ
Выполняем установку сервиса rsyslog на обе ВМ

![IMAGE_DESCRIPTION](УСТАНОВКА_RSYSLOG.png)

Проверим, что сервис функционирует

![IMAGE_DESCRIPTION](STATUS_RSYSLOG.png)

Отредактируем конфигурационные файлы на первой и второй машине

![IMAGE_DESCRIPTION](НАСТРОЙКА_ФАЙЛА_.png)

![IMAGE_DESCRIPTION](НАСТРОЙКА_ФАЙЛА_МАШИНА_2.png)

## Установить и настроить получение логов на сервер с использованием Loki
Установим Loki.
Скачаем конфигурационный файл, архив с утилитой и разархивируем его.

![IMAGE_DESCRIPTION](установка_loki.png)

Зупустим Loki

![IMAGE_DESCRIPTION](запуск_loki.png)

Проверим работоспособоность Loki через браузер

![IMAGE_DESCRIPTION](работа_loki_через_браузер.png)

На второй машине скачаем конфигурационный файл агента Promtail

![IMAGE_DESCRIPTION](promtail.png)

Скачаем архив с утилитой и разархивируем его

![IMAGE_DESCRIPTION](promtail3.png)

Изменим конфигурационный файл 

![IMAGE_DESCRIPTION](редактура_promtail_файла.png)

Запустим Promtail

![IMAGE_DESCRIPTION](запуск_promttail.png)

Проверим работоспособность через браузер

![IMAGE_DESCRIPTION](promtail_браузер.png)

## Работа с SigNoz
Скачаем необходимые файлы для установки 

![IMAGE_DESCRIPTION](установка_signoz.png)

Выполним установку

![IMAGE_DESCRIPTION](запуск_signoz.png)

Зарегестрируемся в signoz с помощью браузера

![IMAGE_DESCRIPTION](создаем_аккаунт_signoz.png)

Просматриваем логи

![IMAGE_DESCRIPTION](logs_SIgnoz.png)

